#!/bin/bash

REGION=$(aws ec2 describe-availability-zones | jq -r .AvailabilityZones[0].RegionName)
AWS_ACCOUNT=$(aws sts get-caller-identity  | jq -r .Account)
RUN_ID=$(date +'%N')
AWS_ROLE="parking-lambda-role-$RUN_ID"
ROLE_POLICY="lambda-ddb-policy-$RUN_ID"
FUNC_NAME="parking-func-$RUN_ID"
API_NAME="parking-api-gateway-$RUN_ID"
DDB_TABLE_NAME="parkinglot-data-$RUN_ID"

echo "Packaging code..."
zip lambda.zip lambda_function.py

echo "Creating DynamoDB table $DDB_TABLE_NAME..."
DDB_TABLE_ARN=$(aws dynamodb create-table \
    --table-name $DDB_TABLE_NAME \
    --attribute-definitions \
        AttributeName=ticketId,AttributeType=S \
    --key-schema \
        AttributeName=ticketId,KeyType=HASH \
    --provisioned-throughput \
        ReadCapacityUnits=5,WriteCapacityUnits=5 \
        | jq -r .TableDescription.TableArn)
    
echo "Creating role $AWS_ROLE..."
aws iam create-role --role-name $AWS_ROLE --assume-role-policy-document file://trust-policy.json

echo "Allowing writes to CloudWatch logs..."
aws iam attach-role-policy --role-name $AWS_ROLE  \
    --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole

echo "Putting inline policy in role..."
TMP_POLICY_FILE=$(mktemp)
jq ".Statement.Resource=\"$DDB_TABLE_ARN\"" role-inline-policy.json > $TMP_POLICY_FILE
aws iam put-role-policy --role-name $AWS_ROLE --policy-name $ROLE_POLICY \
--policy-document=file://$TMP_POLICY_FILE
rm $TMP_POLICY_FILE

echo "Wait for role creation"

aws iam wait role-exists --role-name $AWS_ROLE
aws iam get-role --role-name $AWS_ROLE
AWS_ROLE_ARN=$(aws iam get-role --role-name $AWS_ROLE | jq -r .Role.Arn)

echo "Workaround consistency rules in AWS roles after creation... (sleep 10)"
sleep 10

echo "Creating function $FUNC_NAME..."
aws lambda create-function --function-name $FUNC_NAME \
    --zip-file fileb://lambda.zip --handler lambda_function.lambda_handler \
    --environment=Variables={DYNAMO_TABLE=$DDB_TABLE_NAME} \
    --runtime python3.8 --role $AWS_ROLE_ARN


FUNC_ARN=$(aws lambda get-function --function-name $FUNC_NAME | jq -r .Configuration.FunctionArn)

echo "Creating API Gateway..."
API_CREATED=$(aws apigatewayv2 create-api --name $API_NAME --protocol-type HTTP --target $FUNC_ARN)
API_ID=$(echo $API_CREATED | jq -r .ApiId)
API_ENDPOINT=$(echo $API_CREATED | jq -r .ApiEndpoint)

STMT_ID=$(uuidgen)

aws lambda add-permission --function-name $FUNC_NAME \
    --statement-id $STMT_ID --action lambda:InvokeFunction \
    --principal apigateway.amazonaws.com \
    --source-arn "arn:aws:execute-api:$REGION:$AWS_ACCOUNT:$API_ID/*"

echo "Getting gateway integration id..."
INTEG_ID=$(aws apigatewayv2 get-integrations --api-id=$API_ID | jq -r .Items[0].IntegrationId)

echo "Creating routes and attaching integration"
aws apigatewayv2 create-route --api-id=$API_ID --route-key='POST /entry' --target=integrations/$INTEG_ID
aws apigatewayv2 create-route --api-id=$API_ID --route-key='POST /exit' --target=integrations/$INTEG_ID

echo "Done! API endpoint: $API_ENDPOINT"