import json
import boto3
import os
import math
from datetime import datetime
from uuid import uuid4

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(os.environ.get('DYNAMO_TABLE'))

def lambda_handler(event, context):
    if 'rawPath' in event:
        raw_path = event['rawPath']
        if raw_path == '/entry':
            return handle_entry(event)
        
        if raw_path == '/exit':
            return handle_exit(event)
    
    return {
        'statusCode': 500,
        'headers':{'Content-Type': 'application/json'},
        'body': json.dumps({'error': 'invalid path'})
    }


def handle_entry(event):
    plate = event['queryStringParameters']['plate']
    parking_lot = event['queryStringParameters']['parkingLot']
    ticket = str(uuid4())
    entry_timestamp = str(datetime.now().timestamp())
   
    resp = table.put_item(
        Item={
            'ticketId': ticket,
            'plate': plate,
            'parking_lot': parking_lot,
            'entry_timestamp': entry_timestamp
        }
    )
    
    return {
        'statusCode': 200,
        'headers':{'Content-Type': 'application/json'},
        'body': json.dumps({'ticketId': ticket})
    }


def handle_exit(event):
    ticket = event['queryStringParameters']['ticketId']
    item = table.get_item(Key={'ticketId': ticket})
    
    if 'Item' in item:
        entry_timestamp = float(item['Item']['entry_timestamp'])
        plate = item['Item']['plate']
        parking_lot = item['Item']['parking_lot']
        time_spent = datetime.now().timestamp() - entry_timestamp
        charge = math.ceil((time_spent/60)/15) * 2.5
        
    return {
        'statusCode': 200,
        'headers':{'Content-Type': 'application/json'},
        'body': json.dumps(
                    {
                        'secondsParked': time_spent, 
                        'plate': plate,
                        'parkingLot': parking_lot,
                        'charge': charge
                    }
                )
    }